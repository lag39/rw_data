from scipy.io import loadmat
import h5py

def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """

    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """


    f = h5py.File(infile)

    return f

def output(infile):
    try:
        d = loadmat(infile)
    except:
        d = h5py.File(infile)

    df = dict(d)
    new_dict = {}
    new_dict['x'] = df.get('x')
    new_dict['y'] = df.get('y')
    new_dict['params'] = df.get('params')


    return new_dict

if __name__ == '__main__':
    print(output('mat_v5.mat'))
    print(output('mat_v73.mat'))
